package com.example.lenovopc.mymap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ChooserActivity extends AppCompatActivity {

    private Button current;
    private  Button multipleMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chooser);

        current = (Button) findViewById(R.id.current);
        multipleMarker = (Button) findViewById(R.id.multiple);

        current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent c = new Intent(ChooserActivity.this,MapsActivity.class);
                startActivity(c);
            }
        });


        multipleMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent m = new Intent (ChooserActivity.this,multipleMarkers.class);
                startActivity(m);
            }
        });

    }
}
